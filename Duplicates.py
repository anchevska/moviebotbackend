from pymongo import MongoClient
import tmdbsimple as tmdb
import requests
from bson.objectid import ObjectId


tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieBot
moviesDB = db.movies
actorsDB = db.keywords

def duplicates():
    actors = []
    for actor in actorsDB.find():
        name = actor["name"]
        name = name.replace(" ","")
        if name in actors:
            actorsDB.remove(actor["_id"])
            print("removing {}".format(name))
        else:
            actors.append(name)
    print("done")

def updateFeatureIndex():
    index = 4793
    for actor in actorsDB.find():
        print(index)
        actorsDB.update_one({'_id': actor["_id"]}, {'$set': {"featureIndex": index}})
        index+=1
    print("done")

def addName():
    for actor in actorsDB.find():
        name = actor["name"]
        print(name)
        actorsDB.update_one({'_id': actor["_id"]}, {'$set': {"shortName": name.replace(" ","").lower()}})
    print("done")

addName()
# updateFeatureIndex()
# duplicates()