from pymongo import MongoClient
import tmdbsimple as tmdb
import requests

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieBot
moviesDB = db.movies
actorsDB = db.actors

def updateMovies():
    castDict = {}
    index = 0
    i = 0
    for actor in actorsDB.find():
        castDict[actor["tmdbID"]] = actor["name"]
    for movie in moviesDB.find():
        print(index)
        index+=1
        id = movie["tmdbID"]
        movieID = movie["_id"]
        if id != "":
            tmdbMovie = tmdb.Movies(id)
            r = requests.get(
                f'https://api.themoviedb.org/3/movie/{id}/credits?api_key=6541e072358b81eb11fab7a67a979491')
            if r.status_code != 404:
                credits = tmdbMovie.credits()
                print("movie id {}".format(movieID))
                if "cast" in credits.keys():
                    cast = credits["cast"]
                    for member in cast:
                        id = member["id"]
                        name = member["name"]
                        if id in castDict.keys():
                            print("go ima akterot")
                        else:
                            castDict[id] = name
                            print('inserting {} in index {}'.format(name, i))
                            dict = {"tmdbID": id,
                                    "name": name,
                                    "featureIndex": i
                                    }
                            actorsDB.insert_one(dict)
                            i += 1
                else:
                    print("unable to get cast for movie {}".format(id))
    print("done")

updateMovies()