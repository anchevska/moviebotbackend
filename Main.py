from pymongo import MongoClient
import tmdbsimple as tmdb
import datetime
import requests

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieBot
moviesDB = db.movies
actorsDB = db.actors
directorsDB = db.directors
keywordsDB = db.keywords
genresDB = db.genres

def updateMovies():
    index = 0
    for movie in moviesDB.find():
        print(index)
        index+=1
        id = movie["tmdbID"]
        movieID = movie["_id"]
        print("movie id {}".format(movieID))
        tmdbMovie = tmdb.Movies(id)
        updateMovieWithId(movieID, "movieID", index)
        r = requests.get(
            f'https://api.themoviedb.org/3/movie/{id}?api_key=6541e072358b81eb11fab7a67a979491')
        if r.status_code != 404:
            info = tmdbMovie.info()
            setInfoForMovie(movieID, info)
        r = requests.get(
            f'https://api.themoviedb.org/3/movie/{id}/credits?api_key=6541e072358b81eb11fab7a67a979491')
        if r.status_code != 404:
            credits = tmdbMovie.credits()
            setPeopleForMovie(movieID, credits)
        r = requests.get(
            f'https://api.themoviedb.org/3/movie/{id}/keywords?api_key=6541e072358b81eb11fab7a67a979491')
        if r.status_code != 404:
            keywords = tmdbMovie.keywords()
            setKeywordsForMovie(movieID, keywords)
    print("done")


def setInfoForMovie(id, response):
    print("release date, title, genres and image")
    if "release_date" in response.keys():
        releaseDateString = response["release_date"]
        date = datetime.datetime.strptime(releaseDateString, "%Y-%m-%d")
        updateMovieWithId(id, "release_year", date.year)
    else:
        print("unable to get release date for movie {}".format(id))
    if "title" in response.keys():
        title = response["title"]
        updateMovieWithId(id, "title", title)
    else:
        print("unable to get title for movie {}".format(id))
    if "poster_path" in response.keys():
        imageLink = "https://image.tmdb.org/t/p/w300{}".format(response["poster_path"])
        updateMovieWithId(id, "image", imageLink)
    else:
        print("unable to get poster for movie {}".format(id))
    if "genres" in response.keys():
        genres = response["genres"]
        genresArray = []
        for element in genres:
            index = -1
            for genre in genresDB.find({"name": element["name"]}):
                index = genre["featureIndex"]
            genresArray.append({
                "featureIndex": index,
                "name": element["name"]
            })
        updateMovieWithId(id,"genres",genresArray)
    else:
        print("unable to get genres for movie {}".format(id))



def setKeywordsForMovie(id, response):
    print("keywords")
    if "keywords" in response.keys():
        keywords = response["keywords"]
        keywordsArray = []
        for element in keywords:
            index = -1
            name = element["name"].replace(" ","").lower()
            for keyword in keywordsDB.find({"shortName": name}):
                index = keyword["featureIndex"]
            keywordsArray.append({
                "featureIndex": index,
                "name": element["name"]
            })
        updateMovieWithId(id,"keywords",keywordsArray)
    else:
        print("unable to get keywords for movie {}".format(id))


def setPeopleForMovie(id, response):
    print("cast and crew")
    if "cast" in response.keys():
        cast = response["cast"]
        castArray = []
        for member in cast:
            index = -1
            name = member["name"].replace(" ", "").lower()
            for actor in actorsDB.find({"shortName": name}):
                index = actor["featureIndex"]
            castArray.append({
                "featureIndex":index,
                "name":member["name"]
            })
        updateMovieWithId(id,"actors",castArray)
    else:
        print("unable to get cast for movie {}".format(id))
    if "crew" in response.keys():
        crew = response["crew"]
        for member in crew:
            if member["job"] == "Director":
                index = -1
                name = member["name"].replace(" ", "").lower()
                for director in directorsDB.find({"shortName": name}):
                    index = director["featureIndex"]
                updateMovieWithId(id, "director",{
                    "featureIndex": index,
                    "name": member["name"]
                })
                break
    else:
        print("unable to get crew for movie {}".format(id))


def updateMovieWithId(id, key, value):
    print('update {} with key {} and value {}'.format(id,key,value))
    moviesDB.update_one({'_id': id}, {'$set': {key: value}})

updateMovies()