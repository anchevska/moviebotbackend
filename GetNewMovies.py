from pymongo import MongoClient
import requests
import tmdbsimple as tmdb
import datetime

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.moviesDatabase
moviesDB = db.movies
numberOfMovies = 451720

def importNewMovies():
    index = 435353
    for id in range(index,numberOfMovies):
        print(id)
        tmdbMovie = tmdb.Movies(id)
        r = requests.get(
            f'https://api.themoviedb.org/3/movie/{id}?api_key=6541e072358b81eb11fab7a67a979491')
        if r.status_code != 404:
            response = tmdbMovie.info()
            if "release_date" in response.keys():
                releaseDateString = response["release_date"]
                try:
                    date = datetime.datetime.strptime(releaseDateString, "%Y-%m-%d")
                    if "vote_average" in response.keys() and "vote_count" in response.keys() and "runtime" in response.keys():
                        vote = response["vote_average"]
                        count = response["vote_count"]
                        if date.year >= 1990 and date.year <= 2016 and vote > 7.5 and count > 10 and moviesDB.find(
                                {"tmdbID": str(response["id"])}).count() == 0 and response["runtime"] > 60:
                            moviesDB.insert_one({
                                "imdbID": response["imdb_id"],
                                "tmdbID": str(response["id"]),
                                "title": response["title"]
                            })
                            print(response["title"])
                            print(date.year)
                except ValueError:
                    print("no date")
            else:
                print("unable to get release date for movie {}".format(id))
    print("done")


importNewMovies()