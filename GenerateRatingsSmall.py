from pymongo import MongoClient
import tmdbsimple as tmdb

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieRecommenderBot
fullDB = client.movieBot
moviesDB = db.movies
ratingsDB = fullDB.ratings
ratingsSmallDB = db.ratings

def generateRatingsSmall():
    index = 0
    for ratingDict in ratingsDB.find():
        print(index)
        ratingsSmall = []
        ratingsArray = ratingDict["ratings"]
        for ratingDict in ratingsArray:
            movieID = ratingDict["movieID"]
            cursor = moviesDB.find({"movieID":movieID})
            if cursor.count() > 0:
                ratingsSmall.append(ratingDict)
        ratingsSmallDB.insert_one({"ratings":ratingsSmall})
        index+=1

generateRatingsSmall()