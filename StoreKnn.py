import numpy
import math
from pymongo import MongoClient

client = MongoClient()
db = client.movieBot
moviesDB = db.movies
ratingsDB = db.ratingsFinal
similaritiesDatabase = db.similarities
numberMovies = 9125
numberNearestNeighbours = 30
similaritiesMatrix = numpy.full((math.ceil(numberMovies/2), numberMovies), numpy.inf)
ratingsMatrix = numpy.zeros((1, 9125), dtype=float)
predictionsMatrix = numpy.zeros((1, 9125), dtype=float)
movieTitles = ["" for x in range(numberMovies)]

def getMovies():
    print("reading movies")
    for index, movie in enumerate(moviesDB.find()):
        movieTitles[index] = movie["title"]

def calculateKnnForItem(itemIndex):
    itemSimilaritiesArray = similaritiesMatrix[itemIndex]
    for i in range(numberNearestNeighbours):
        maxSimilarity = max(itemSimilaritiesArray)
        print(maxSimilarity)
        indexOfMax = itemSimilaritiesArray.index(maxSimilarity)
        print(indexOfMax)


def init():
    readAndStoreSimilarities()
    for itemIndex in range(numberMovies):
        calculateKnnForItem(itemIndex)
        break

def findRatingsForUser(user):
    for index in range(ratingsMatrix.shape[1]):
        if ratingsMatrix[user][index] == 0:
            findRating(user,index)
    for index in range(9125):
        print(predictionsMatrix[user][index])

def findRating(user,item):
    sumSimilarities = 0
    sumNominator = 0
    userRatings = ratingsMatrix[user]
    for index , rating in enumerate(userRatings):
        if rating != 0:
            similarity = getSimilarity(item, index)
            if similarity > 0:
                sumNominator += similarity * rating
                sumSimilarities += similarity
    if sumSimilarities == 0:
        predictionsMatrix[user][item] = -1
    else:
        predictionsMatrix[user][item] = sumNominator/sumSimilarities

def getSimilarity(item1, item2):
    return similaritiesMatrix[min(item1,item2)][max(item1,item2)]

def readAndStoreSimilarities():
    print("reading similarities")
    index = 0
    for similaritiesArray in similaritiesDatabase.find().limit(math.ceil(numberMovies/2)):
        similaritiesMatrix[index] = similaritiesArray["similarityItems"]
        index += 1

init()