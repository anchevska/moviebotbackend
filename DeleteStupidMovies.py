from pymongo import MongoClient

client = MongoClient()
db = client.movieRecommenderBot
moviesDB = db.movies
ratingsDB = db.allRatings

def deleteMovies():
    index = 0
    for movie in moviesDB.find():
        print(index)
        cursor = ratingsDB.find({"movieID": movie})
        if cursor.count() >= 30:
            moviesDB.insert_one(movie)
        index+=1
    print("done")

def deleteOldMovies():
    for movie in moviesDB.find():
        if "release_year" in movie.keys() and movie["release_year"] >= 1990:
            print(movie["title"])
            client.latestDatabase.movies.insert_one(movie)
    print("done")

# deleteMovies()
deleteOldMovies()