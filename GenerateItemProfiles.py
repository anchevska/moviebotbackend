from pymongo import MongoClient
import tmdbsimple as tmdb
import numpy
from bson import ObjectId

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieBot
moviesDB = db.movies
actorsDB = db.actors
directorsDB = db.directors
keywordsDB = db.keywords
genresDB = db.genres
itemProfilesDB = db.itemProfilesVectors

def generateItemProfiles():
    index = 0
    numberFeatures = 6334
    for movie in moviesDB.find():
        print(index)
        item = []
        # for i in range(numberFeatures):
        #     item.append(0)
        # if "release_year" in movie.keys():
        #     year = movie["release_year"]
        #     item.append(year)
        if "actors" in movie.keys():
            for actor in movie["actors"]:
                if isinstance(actor,dict) and "featureIndex" in actor.keys():
                    actorIndex = actor["featureIndex"]
                    if actorIndex <= numberFeatures:
                        item.append(actorIndex)
        if "keywords" in movie.keys():
            for keyword in movie["keywords"]:
                if isinstance(keyword,dict) and "featureIndex" in keyword.keys():
                    keywordIndex = keyword["featureIndex"]
                    if keywordIndex <= numberFeatures:
                        item.append(keywordIndex)
        if "genres" in movie.keys():
            for genre in movie["genres"]:
                if isinstance(genre,dict) and "featureIndex" in genre.keys():
                    genreIndex = genre["featureIndex"]
                    if genreIndex <= numberFeatures:
                        item.append(genreIndex)
        if "director" in movie.keys():
            directorDict = movie["director"]
            if isinstance(directorDict,dict) and "featureIndex" in directorDict.keys():
                directorIndex = directorDict["featureIndex"]
                if directorIndex <= numberFeatures:
                    item.append(directorIndex)
        itemProfilesDB.insert_one({"index": index,
                                   "vector":item,
                                   'movieID': movie["movieID"]})
        index+=1
    print("done")


generateItemProfiles()