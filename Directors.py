from pymongo import MongoClient
import tmdbsimple as tmdb
import requests

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieBot
moviesDB = db.movies
actorsDB = db.directors

def updateMovies():
    castDict = {}
    index = 0
    i = 4575
    # for director in actorsDB.find():
    #     castDict[director["tmdbID"]] = director["name"]
    # print(castDict)
    for movie in moviesDB.find():
        print(index)
        index+=1
        id = movie["tmdbID"]
        movieID = movie["_id"]
        if id != "":
            tmdbMovie = tmdb.Movies(id)
            r = requests.get(
                f'https://api.themoviedb.org/3/movie/{id}/credits?api_key=6541e072358b81eb11fab7a67a979491')
            if r.status_code != 404:
                credits = tmdbMovie.credits()
                print("movie id {}".format(movieID))
                if "crew" in credits.keys():
                    cast = credits["crew"]
                    for member in cast:
                        if member["job"] == "Director":
                            id = member["id"]
                            name = member["name"]
                            print(name)
                            if id in castDict.keys():
                                print("go ima reziserot")
                            else:
                                print('inserting {} in index {}'.format(name, i))
                                castDict[id] = name
                                dict = {"tmdbID": id,
                                        "name": name,
                                        "featureIndex": i
                                        }
                                actorsDB.insert_one(dict)
                                i+=1
                            break
                else:
                    print("unable to get cast for movie {}".format(id))
    print("done")

updateMovies()