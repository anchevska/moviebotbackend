from pymongo import MongoClient
import tmdbsimple as tmdb
import numpy

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieRecommenderBot
moviesDB = db.movies
ratingsDB = db.ratings
allRatingsDB = db.allRatings

def createAllRatings():
    i = 0
    for userRatings in ratingsDB.find():
        print("user {}".format(i))
        ratingObjects = userRatings["ratings"]
        id = userRatings["_id"]
        for ratingObject in ratingObjects:
            allRatingsDB.insert_one({
                "itemNumber":ratingObject["itemNumber"],
                "movieID":ratingObject["movieID"],
                "userID":id,
                "rating":ratingObject["rating"]
            })
        i+=1
    print("done")

createAllRatings()