from pymongo import MongoClient
import tmdbsimple as tmdb
import requests

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieBot
moviesDB = db.movies
actorsDB = db.genres

def updateMovies():
    castDict = {}
    index = 0
    i = 6315
    # for director in actorsDB.find():
    #     castDict[director["tmdbID"]] = director["name"]
    # print(castDict)
    for movie in moviesDB.find():
        print(index)
        index+=1
        id = movie["tmdbID"]
        movieID = movie["_id"]
        if id != "":
            tmdbMovie = tmdb.Movies(id)
            r = requests.get(
                f'https://api.themoviedb.org/3/movie/{id}?api_key=6541e072358b81eb11fab7a67a979491')
            if r.status_code != 404:
                info = tmdbMovie.info()
                print("movie id {}".format(movieID))
                if "genres" in info.keys():
                    for member in info["genres"]:
                        id = member["id"]
                        name = member["name"]
                        print(name)
                        if id in castDict.keys():
                            print("go ima")
                        else:
                            print('inserting {} in index {}'.format(name, i))
                            castDict[id] = name
                            dict = {
                                    "name": name,
                                    "featureIndex": i
                                    }
                            actorsDB.insert_one(dict)
                            i += 1

                else:
                    print("unable to get cast for movie {}".format(id))
    print(castDict)
    print("done")

updateMovies()