from flask import Flask, jsonify, request
from pymongo import MongoClient
from bson import ObjectId
import numpy
from scipy import spatial
import operator

app = Flask(__name__)

# client = MongoClient("mongodb://viktorija:test@ds145389.mlab.com:45389/movie-bot")
# db = client["movie-bot"]
client = MongoClient()
db = client.movieBot
moviesDB = db.movies
choicesDB = db.choices
scenariosDB = db.scenarios
talkDB = db.talk
allRatingsDB = db.allRatings
ratingsDB = db.ratings
item_profilesDB = db.itemProfilesVectors
user_profilesDB = db.userProfilesVectors
usersDB = db.users
numberFeatures = 6334


# @app.route('/allMovies', methods=['GET'])
def get_all_movies():
    movies_array = []
    for movie in moviesDB.find():
        movies_array.append(dict(
            title=movie["title"],
            movieId=str(movie["_id"]),
            movieID=movie["movieID"],
            imdbID=movie["imdbID"],
            releaseYear=movie["release_year"],
            tmdbID=movie["tmdbID"]))
        if "image" in movie.keys():
            movies_array[len(movies_array)-1]["imageLink"] = movie["image"]
    return movies_array


@app.route('/movies', methods=['GET'])
def get_movies():
    pipeline = [
        {"$group": {"_id": "$itemNumber", "ratings": {"$push": "$rating"}, "averageRating": {"$avg": "$rating"},
                    "size": {"$sum": 1}}},
        {"$sort": {"size": -1}}]
    list_most_rated_movies = list(allRatingsDB.aggregate(pipeline))
    movies_dict = {}
    movies_list = []
    i = 0
    for item in list_most_rated_movies:
        if item["averageRating"] > 4:
            i += 1
            movie = moviesDB.find_one({"itemNumber": item["_id"]})
            movies_dict[str(movie["_id"])] = dict(
                title=movie["title"],
                itemNumber=movie["itemNumber"],
                movie_id=str(movie["_id"]),
                imdbID=movie["imdbID"],
                tmdbID=movie["tmdbID"],
                genres=movie["genres"],
                imageLink=movie["image"],
                averageRating=item["averageRating"])
            if "imageLink" in movie.keys():
                movies_dict[str(movie["_id"])]["imageLink"] = movie["imageLink"]
            if movie["release_year"] >= 2000:
                movies_list.append(movies_dict[str(movie["_id"])])
            # if i == 100:
            #     break
    return jsonify(movies_list)


@app.route('/getUser', methods=['POST'])
def get_user():
    if not request.json or 'uid' not in request.json:
        return jsonify(), 400
    if usersDB.find({"uid": request.json['uid']}).count() <= 0:
        print("user does not exist")
        user = {
            'uid': request.json['uid'],
            "checkedMovies": []
        }
        result = usersDB.insert_one(user)
        choice = {
            "_id": result.inserted_id,
            "choices": [
                {
                    "descriptions": ["Anybody here?", "Anybody there?"],
                    "link": "onboarding-start"
                }
            ]
        }
        choicesDB.insert_one(choice)
        return jsonify({"_id": str(result.inserted_id)}), 201
    else:
        user_id = ""
        for user in usersDB.find({"uid": request.json['uid']}).limit(1):
            user_id = str(user["_id"])
        return jsonify({"_id": user_id}), 201


@app.route('/addRating', methods=['POST'])
def add_rating():
    if not request.json or 'uid' not in request.json or 'movieID' not in request.json or 'rating' not in request.json:
        return jsonify(), 400
    else:
        movie_id = request.json['movieID']
        rating_number = request.json['rating'] / 1.0
        features = []
        next_movie = {}
        for moviesFound in item_profilesDB.find({"movieID": movie_id}):
            features = moviesFound["vector"]
        rating = {
            'movie_id': movie_id,
            'rating': rating_number,
            "features": features
        }
        for userID in usersDB.find({"uid": request.json['uid']}).limit(1):
            # Object id for user with uid
            mongo_id_for_user = str(userID["_id"])
            obj_id_for_user = ObjectId(mongo_id_for_user)
            cursor = allRatingsDB.find({"userID": mongo_id_for_user, "movie_id": movie_id})
            if cursor.count() > 0:
                # user rated this movie
                print("user rated this movie")
                for current_user_ratings in ratingsDB.find({"_id": obj_id_for_user}).limit(1):
                    curr_user_ratings = current_user_ratings["ratings"]
                    for index, ratingDictionary in enumerate(curr_user_ratings):
                        if ratingDictionary["movie_id"] == movie_id:
                            curr_user_ratings[index] = rating
                    ratingsDB.update_one({'_id': obj_id_for_user}, {'$set': {'ratings': curr_user_ratings}})
                    rating["userID"] = mongo_id_for_user
                    allRatingsDB.update_one(
                        {'userID': mongo_id_for_user, "movie_id": movie_id},
                        {'$set': {'rating': rating_number}})
            else:
                # user didn't rate this movie
                print("user didn't rate this movie")
                if ratingsDB.find({"_id": obj_id_for_user}).limit(1).count() > 0:
                    ratingsDB.update_one({'_id': obj_id_for_user}, {'$push': {'ratings': rating}})
                else:
                    ratingsDB.insert_one({'_id': obj_id_for_user, 'ratings': [rating]})
                rating["userID"] = mongo_id_for_user
                allRatingsDB.insert_one(rating)
            update_user_weights(mongo_id_for_user, features)
            usersDB.update_one({'_id': obj_id_for_user}, {'$push': {'checkedMovies': movie_id}})
            next_movie = get_next_movie_for_user(obj_id_for_user)
    return jsonify(next_movie), 201


@app.route('/notInterested', methods=['POST'])
def not_interested():
    if not request.json or 'uid' not in request.json or 'movieID' not in request.json:
        return jsonify(), 400
    else:
        movie_id = request.json['movieID']
        next_movie = {}
        for userID in usersDB.find({"uid": request.json['uid']}).limit(1):
            # Object id for user with uid
            mongo_id_for_user = str(userID["_id"])
            obj_id_for_user = ObjectId(mongo_id_for_user)
            usersDB.update_one({'_id': obj_id_for_user}, {'$push': {'checkedMovies': movie_id}})
            next_movie = get_next_movie_for_user(obj_id_for_user)
    return jsonify(next_movie), 201


def get_next_movie_for_user(user_id):
    all_movies = get_all_movies()
    for userDict in usersDB.find({"_id": user_id}).limit(1):
        checked_movies = userDict["checkedMovies"]
        for movie in all_movies:
            if movie["movieID"] not in checked_movies:
                movie["source"] = 2
                talkDB.update_one({'_id': user_id}, {'$pop': {'messages': 1}})
                talkDB.update_one({'_id': user_id}, {'$push': {'messages': movie}})
                return movie


def update_user_weights(user_id, features_for_movie):
    ratings = []
    for userRatings in ratingsDB.find({"_id": ObjectId(user_id)}).limit(1):
        ratings = userRatings["ratings"]
    cursor = user_profilesDB.find({"_id": ObjectId(user_id)}).limit(1)
    if cursor.count() <= 0:
        # user doesn't have a profile
        print("user doesn't have a profile")
        weights = get_dictionary_for_updated_features(features_for_movie, ratings)
        user_profilesDB.insert_one({"_id": ObjectId(user_id),
                                   "weights": weights})
    else:
        # user has profile
        print("user has profile")
        for user_profile in cursor:
            user_features = []
            for weight in user_profile["weights"]:
                user_features.append(weight["featureIndex"])
            all_features = concatenate_dictionaries(user_features, features_for_movie)
            weights = get_dictionary_for_updated_features(all_features, ratings)
            user_profilesDB.update_one({'_id': ObjectId(user_id)}, {'$set': {'weights': weights}})
            

def get_dictionary_for_updated_features(features, ratings):
    average_user_rating = calculate_average_rating_for_user(ratings)
    features_weights = []
    for feature in features:
        weight = 0
        common_movies = get_movies_user_rated_and_have_feature(ratings, feature)
        for movie in common_movies:
            weight += movie["rating"] - average_user_rating
        features_weights.append({"featureIndex": feature, "weight": weight})
    return features_weights


def get_movies_user_rated_and_have_feature(ratings, feature):
    final_movies = []
    for ratingDict in ratings:
        if feature in ratingDict["features"]:
            final_movies.append(ratingDict)
    return final_movies


def calculate_average_rating_for_user(ratings):
    average = 0
    for ratingDict in ratings:
        rating = ratingDict["rating"]
        average += rating
    average /= len(ratings)
    return average


def concatenate_dictionaries(array1, array2):
    array = array2
    for item1 in array1:
        if item1 not in array2:
            array.append(item1)
    return array


@app.route('/getRecommendations', methods=['POST'])
def get_recommendations():
    if not request.json or 'uid' not in request.json:
        return jsonify(), 400
    cursor = usersDB.find({"uid": request.json['uid']}).limit(1)
    if cursor.count() > 0:
        user_profile = numpy.zeros(numberFeatures)
        item_profiles_dict = {}
        for movieProfile in item_profilesDB.find().skip(1):
            item_profile = numpy.zeros(numberFeatures)
            for index, feature in enumerate(movieProfile["vector"]):
                item_profile[feature] = 1
            item_profiles_dict[movieProfile["movie_id"]] = item_profile
        user_id = ""
        for user in cursor:
            user_id = ObjectId(user["_id"])
        for userDict in user_profilesDB.find({"_id": user_id}).limit(1):
            for weightDict in userDict["weights"]:
                user_profile[weightDict["featureIndex"]] = weightDict["weight"]
        distances = {}
        for key in item_profiles_dict.keys():
            item_profile = item_profiles_dict[key]
            distance = spatial.distance.cosine(item_profile, user_profile)
            distances[key] = distance
        sorted_x = sorted(distances.items(), key=operator.itemgetter(1))
        idx = 0
        response = {}
        response_list = []
        for item in sorted_x:
            if idx == 150:
                break
            else:
                idx += 1
                for movie in moviesDB.find({"movie_id": item[0]}):
                    response[str(movie["_id"])] = dict(
                        title=movie["title"],
                        itemNumber=movie["itemNumber"],
                        movie_id=str(movie["_id"]),
                        imdbID=movie["imdbID"],
                        tmdbID=movie["tmdbID"],
                        genres=movie["genres"],
                        imageLink=movie["image"])
                    if "imageLink" in movie.keys():
                        response[str(movie["_id"])]["imageLink"] = movie["imageLink"]
                    if movie["release_year"] >= 1990:
                        response_list.append(response[str(movie["_id"])])
                        print(movie["title"])
        return jsonify(response_list), 201
    else:
        print("user does not exist")
        return jsonify(), 400


@app.route('/getChoices', methods=['POST'])
def get_choices():
    if not request.json or 'id' not in request.json:
        return jsonify(), 400
    choices_array = []
    configuration = 2
    for choicesEntry in choicesDB.find({"_id": ObjectId(request.json['id'])}):
        for choice in choicesEntry["choices"]:
            choices_array.append(choice)
        if "configuration" in choicesEntry:
            configuration = choicesEntry["configuration"]
    return jsonify({"choices": choices_array, "configuration": configuration}), 201


@app.route('/redirectScenario', methods=['POST'])
def redirect_scenario():
    if not request.json or 'id' not in request.json or 'link' not in request.json or 'title' not in request.json:
        return jsonify(), 400
    choices_array = []
    messages_array = []
    user_id = ObjectId(request.json['id'])
    scenario_has_type = 0
    state_type = ""
    type_dict = {}
    configuration = 2
    # get next choices and messages
    for scenario in scenariosDB.find({"id": request.json['link']}).limit(1):
        if "choices" in scenario.keys():
            choices_array = scenario["choices"]
        if "messages" in scenario.keys():
            messages_array = scenario["messages"]
        if "type" in scenario.keys():
            state_type = scenario["type"]
            type_dict = check_state_type(state_type, user_id)
            if type_dict != {}:
                scenario_has_type = 1
        if "configuration" in scenario.keys():
            configuration = scenario["configuration"]
    # update new choices for the user
    for _ in choicesDB.find({"_id": user_id}).limit(1):
        choicesDB.update_one({'_id': user_id}, {'$set': {'choices': choices_array}})
        choicesDB.update_one({'_id': user_id}, {'$set': {'configration': configuration}})
    # update new messages for the user
    cursor = talkDB.find({"_id": user_id}).limit(1)
    if cursor.count() <= 0:
        insert_choice = {
            "_id": user_id,
            "messages": [{"message": request.json["title"], "source": 1}]
        }
        talkDB.insert_one(insert_choice)
    else:
        # talkDB.update_one({'_id': user_id}, {'$push': {'messages': {"message": request.json["title"], "source": 1}}})
        title = request.json["title"]
        if title != "":
            talkDB.update_one({'_id': user_id}, {'$push': {'messages': {"message": request.json["title"], "source": 1}}})
    for message in messages_array:
        talkDB.update_one({'_id': user_id}, {'$push': {'messages': message}})
    if scenario_has_type:
        type_dict["source"] = 2
        talkDB.update_one({'_id': user_id}, {'$push': {'messages': type_dict}})
        messages_array.append(type_dict)
        return jsonify({"choices": choices_array, "messages": messages_array, "type": state_type, "configuration": configuration}), 201
    else:
        return jsonify({"choices": choices_array, "messages": messages_array, "configuration": configuration}), 201


def check_state_type(state_type, user_id):
    if state_type == "showMovies":
        return get_next_movie_for_user(user_id)
    return {}


@app.route('/getInitialMessages', methods=['POST'])
def get_initial_messages():
    if not request.json or 'id' not in request.json:
        return jsonify(), 400
    messages_array = []
    for talk_entry in talkDB.find({"_id": ObjectId(request.json['id'])}):
        for message in talk_entry["messages"]:
            messages_array.append(message)
    return jsonify(messages_array), 201

if __name__ == '__main__':
    app.run(debug=True)
