from pymongo import MongoClient
import tmdbsimple as tmdb
import numpy
from bson import ObjectId

tmdb.API_KEY = '6541e072358b81eb11fab7a67a979491'
client = MongoClient()
db = client.movieRecommenderBot
moviesDB = db.movies
ratingsDB = db.ratings
itemProfilesDB = db.itemProfilesVectors

def addFeaturesInRatings():
    index = 0
    for userRating in ratingsDB.find():
        print(index)
        newRatings = []
        for ratingDictionary in userRating["ratings"]:
            movieID = ratingDictionary["movieID"]
            for moviesFound in itemProfilesDB.find({"movieID":movieID}):
                ratingDictionary["features"] = moviesFound["vector"]
            newRatings.append(ratingDictionary)
        ratingsDB.update_one({'_id': ObjectId(userRating["_id"])}, {'$set': {'ratings': newRatings}})
        index += 1
    print("done")

addFeaturesInRatings()